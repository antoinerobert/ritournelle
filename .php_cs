<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('DataFixtures')
    ->exclude('Migrations')
    ->in('src')
;

return PhpCsFixer\Config::create()
    ->setRules([
        '@Symfony' => true,
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
