<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomResultRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CustomResult
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CustomRitournelle", inversedBy="customResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customRitournelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="customResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $replyAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomRitournelle(): ?CustomRitournelle
    {
        return $this->customRitournelle;
    }

    public function setCustomRitournelle(?CustomRitournelle $customRitournelle): self
    {
        $this->customRitournelle = $customRitournelle;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getReplyAt(): ?\DateTimeImmutable
    {
        return $this->replyAt;
    }

    public function setReplyAt(\DateTimeImmutable $replyAt): self
    {
        $this->replyAt = $replyAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @throws \Exception
     */
    public function setReplyAtValue()
    {
        $this->replyAt = new \DateTimeImmutable();
    }
}
