<?php

namespace App\Entity;

use App\Service\NoDiacritic;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RitournelleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Ritournelle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $enigma;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $response;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private $clue;

    /**
     * @var Game[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Game", mappedBy="ritournelle", orphanRemoval=true)
     */
    private $games;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled = true;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private $createdAt;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="ritournelles")
     */
    private $user;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnigma(): ?string
    {
        return $this->enigma;
    }

    public function setEnigma(string $enigma): self
    {
        $this->enigma = $enigma;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(string $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getClue(): ?string
    {
        return $this->clue;
    }

    public function setClue(string $clue): self
    {
        $this->clue = $clue;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->setRitournelle($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->contains($game)) {
            $this->games->removeElement($game);
            // set the owning side to null (unless already changed)
            if ($game->getRitournelle() === $this) {
                $game->setRitournelle(null);
            }
        }

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): Ritournelle
    {
        $this->user = $user;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): Ritournelle
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @throws \Exception
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function isValidResponse(string $response): bool
    {
        if (empty(trim($response))) {
            return false;
        }

        return 0 === strcasecmp(
                $this->clean($response),
                $this->clean($this->response)
            );
    }

    public function getRating(): ?int
    {
        if (empty($this->games)) {
            return null;
        }
        $rating = 0;
        $nbResults = 0;
        foreach ($this->games as $game) {
            $results = $game->getResults();
            $nbResults += $results->count();
            foreach ($results as $result) {
                $rating += $result->getRating();
            }
        }
        if (0 == $nbResults) {
            return null;
        }

        return ceil($rating / $nbResults);
    }

    public function getLastRating(): ?int
    {
        $lastGame = $this->getLastGame();
        if (empty($lastGame)) {
            return null;
        }
        $results = $lastGame->getResults();
        if (0 == $results->count()) {
            return null;
        }
        $rating = 0;
        foreach ($results as $result) {
            $rating += $result->getRating();
        }

        return ceil($rating / $results->count());
    }

    private function getLastGame(): ?Game
    {
        /** @var Game|null $lastGame */
        $lastGame = null;
        if (empty($this->games)) {
            return $lastGame;
        }
        foreach ($this->games as $game) {
            if (empty($lastGame) || $lastGame->getPlayedAt() < $game->getPlayedAt()) {
                $lastGame = $game;
            }
        }

        return $lastGame;
    }

    /**
     * On renvoie la moitié des mots aléatoirement.
     */
    public function getRandomHalfWords(): array
    {
        $randomWords = [];
        $words = explode(' ', $this->response);
        $nbWords = floor(count($words) / 2);

        $safeguard = 100;
        for ($i = 0; $i < $nbWords && $safeguard > 0; ++$i) {
            $randomWord = $words[array_rand($words)];
            if (in_array($randomWord, $randomWords)) {
                --$i;
                --$safeguard;
                continue;
            }
            $randomWords[] = $randomWord;
        }

        return $randomWords;
    }

    public function getSimilarity(string $trial): float
    {
        similar_text(
            $this->clean($trial),
            $this->clean($this->response),
            $percent);

        return $percent;
    }

    private function clean(string $text): string
    {
        return trim(preg_replace('/[\'"´’.,;:\/\!§%$£*¤"#\'`\(){}+=@&]/', '', NoDiacritic::filter($text)));
    }
}
