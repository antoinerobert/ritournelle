<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JokerPlayedRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class JokerPlayed
{
    const JOKER_SIMILARITY = 1;
    const JOKER_RANDOM_WORD = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $joker;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="jokerPlayeds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Game", inversedBy="jokerPlayeds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;

    /**
     * @ORM\Column(type="string", length=2048)
     */
    private $clue;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $playedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoker(): ?int
    {
        return $this->joker;
    }

    public function setJoker(int $joker): self
    {
        $this->joker = $joker;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getClue(): ?string
    {
        return $this->clue;
    }

    public function setClue(string $clue): self
    {
        $this->clue = $clue;

        return $this;
    }

    public function getPlayedAt(): ?\DateTimeImmutable
    {
        return $this->playedAt;
    }

    public function setPlayedAt(\DateTimeImmutable $playedAt): self
    {
        $this->playedAt = $playedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * @throws \Exception
     */
    public function setPlayedAtValue()
    {
        $this->playedAt = new \DateTimeImmutable();
    }
}
