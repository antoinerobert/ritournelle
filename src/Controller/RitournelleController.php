<?php

namespace App\Controller;

use App\Entity\CustomResult;
use App\Entity\CustomRitournelle;
use App\Entity\Game;
use App\Entity\Response as ResponseGame;
use App\Entity\Result;
use App\Entity\Ritournelle;
use App\Entity\User;
use App\Form\CommentType;
use App\Form\CustomResponseType;
use App\Form\CustomRitournelleType;
use App\Form\ResponseType;
use App\Form\RitournelleType;
use App\Repository\CustomRitournelleRepository;
use App\Repository\GameRepository;
use App\Repository\ResultRepository;
use App\Repository\RitournelleRepository;
use App\Service\GameManager;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ritournelle", name="ritournelle_")
 */
class RitournelleController extends AbstractController
{
    /**
     * @Route("/enigma", name="enigma")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function enigma(
        EntityManagerInterface $em,
        GameManager $gameManager,
        Request $request
    ) {
        /** @var User $user */
        $user = $this->getUser();

        $game = $gameManager->getTodaysGame();
        $ritournelle = $game->getRitournelle();

        /** @var ResultRepository $resultRepo */
        $resultRepo = $em->getRepository(Result::class);
        $result = $resultRepo->findTodayResult($user, $game);
        $results = $game->getResults();

        $customRitournelleFormView = $previousRitournelle = null;

        // on cherche les informations pour les customs ritournelles
        $previousGame = $gameManager->getPreviousGame();
        if ($previousGame) {
            $previousRitournelle = $previousGame->getRitournelle();
        }
        /** @var CustomRitournelleRepository $customRitournelleRepo */
        $customRitournelleRepo = $em->getRepository(CustomRitournelle::class);
        $customRitournelle = $customRitournelleRepo->findOneBy(['user' => $user, 'createdAt' => new DateTimeImmutable('today')]);
        if (!$customRitournelle) {
            $customRitournelle = new CustomRitournelle();
            $customRitournelleForm = $this->createForm(CustomRitournelleType::class, $customRitournelle);
            $customRitournelleForm->handleRequest($request);
            if ($customRitournelleForm->isSubmitted() && $customRitournelleForm->isValid()) {
                if (empty($result)) {
                    $this->addFlash('success', 'success.defy_ritournelle_waiter');
                } else {
                    $this->addFlash('success', 'success.defy_ritournelle_winner');
                }
                $customRitournelle->setUser($user);
                $em->persist($customRitournelle);
                $em->flush();

                return $this->redirectToRoute('ritournelle_enigma');
            }
            // On n'envoie pas le formulaire si une ritournelle a déjà été envoyée
            $customRitournelleFormView = $customRitournelleForm->createView();
        }
        $customResults = $customRitournelle->getCustomResults();

        // On récupère les ritournelles des autres
        $customRitournelleOthers = $customRitournelleRepo->findOthers(new DateTimeImmutable('today'), $user);
        $customOthers = [];
        foreach ($customRitournelleOthers as $customRitournelleOther) {
            $customOther = [];
            $customOther['ritournelle'] = $customRitournelleOther;
            $customOther['result'] = $customRitournelleOther->getCustomResult($user) ?? '';
            $customOther['form'] = '';

            if (empty($customOther['result'])) {
                $customResponse = new \stdClass();
                $customResponse->customRitournelle = $customRitournelleOther;
                $customResponse->response = '';
                $customResponseForm = $this->createForm(CustomResponseType::class, $customResponse);

                $customResponseForm->handleRequest($request);
                if ($customResponse->customRitournelle === $customRitournelleOther && $customResponseForm->isSubmitted() && $customResponseForm->isValid()) {
                    // on vérifie si la réponse donnée correspond à celle de la ritournelle
                    if ($customRitournelleOther->isValidResponse($customResponse->response)) {
                        // On enregistre en BDD que la réponse a été trouvé
                        $customResult = new CustomResult();
                        $customResult->setUser($user);
                        $customResult->setCustomRitournelle($customRitournelleOther);
                        $em->persist($customResult);
                        $em->flush();

                        // On indique que la réponse en juste
                        $this->addFlash('success', 'ritournelle.right_custom_response');

                        return $this->redirectToRoute('ritournelle_enigma');
                    } else {
                        // on indique que la réponse est fausse
                        $this->addFlash('danger', 'ritournelle.wrong_custom_response');
                    }
                }
                $customResponse->customRitournelle = $customRitournelleOther;
                $customResponse->response = '';
                $customResponseForm = $this->createForm(CustomResponseType::class, $customResponse);
                $customOther['form'] = $customResponseForm->createView();
            }
            $customOthers[] = $customOther;
        }

        // Si la personne n'a toujours pas trouvé, on regarde si elle a soumis le formulaire
        $response = new \stdClass();
        $response->enigma = $ritournelle->getEnigma();
        $response->response = '';
        $responseForm = $this->createForm(ResponseType::class, $response);
        if (empty($result)) {
            $responseForm->handleRequest($request);
            if ($responseForm->isSubmitted() && $responseForm->isValid()) {
                // on vérifie si la réponse donnée correspond à celle de la ritournelle
                if ($ritournelle->isValidResponse($response->response)) {
                    // On enregistre en BDD que la réponse a été trouvé
                    $result = $resultRepo->save($game, $user);
                    // on ajoute à la liste
                    $results[] = $result;

                    // On indique que la réponse en juste
                    $this->addFlash('success', 'ritournelle.right_response');

                    // On va vers un affichage qui gère l'ajout d'un commentaire
                    return $this->redirectToRoute('ritournelle_comment');
                } else {
                    $responseGame = new ResponseGame();
                    $responseGame->setGame($game);
                    $responseGame->setUser($user);
                    $responseGame->setResponse($response->response);

                    $em->persist($responseGame);
                    $em->flush();
                }

                // On indique que la réponse est fausse
                $this->addFlash('danger', 'ritournelle.wrong_response');
            }
        }
        $responseFormView = $responseForm->createView();

        return $this->render(
            'ritournelle/enigma.html.twig',
            compact(
                'customOthers',
                'customResults',
                'customRitournelle',
                'customRitournelleFormView',
                'game',
                'previousRitournelle',
                'responseFormView',
                'result',
                'results',
                'ritournelle',
            )
        );
    }

    /**
     * @Route("/comment", name="comment")
     *
     * @return Response
     *
     * @throws Exception
     */
    public function commentResult(Request $request, EntityManagerInterface $em)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var GameRepository $gameRepo */
        $gameRepo = $em->getRepository(Game::class);
        $game = $gameRepo->getTodaysGame();

        /** @var ResultRepository $resultRepo */
        $resultRepo = $em->getRepository(Result::class);
        $result = $resultRepo->findTodayResult($user, $game);
        $results = $resultRepo->findByGame($game);

        if (!$result) {
            return $this->redirectToRoute('ritournelle_enigma');
        }

        $comment = new \stdClass();
        $comment->comment = $result->getComment();
        $comment->rating = $rating = $result->getRating() ?? Result::RATING_DEFAULT;
        $comment->comment_ritournelle = $result->getCommentRitournelle();
        $commentForm = $this->createForm(CommentType::class, $comment);

        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $result->setComment($comment->comment);
            $result->setRating($comment->rating);
            $result->setCommentRitournelle($comment->comment_ritournelle);
            $em->persist($result);
            $em->flush();

            return $this->redirectToRoute('ritournelle_enigma');
        }
        $commentFormView = $commentForm->createView();

        return $this->render(
            'ritournelle/comment.html.twig',
            compact(
                'commentFormView',
                'results',
                'rating',
            ));
    }

    /**
     * @Route("/list", name="list")
     * @IsGranted("ROLE_ADMIN")
     */
    public function list(EntityManagerInterface $em)
    {
        /** @var RitournelleRepository $ritournelleRepo */
        $ritournelleRepo = $em->getRepository(Ritournelle::class);
        $ritournelles = $ritournelleRepo->findAll();

        return $this->render('ritournelle/list.html.twig', compact('ritournelles'));
    }

    /**
     * @Route("/add/{id}", name="add", requirements={"id"="\d+"}, defaults={"id"=0})
     *
     * @return Response
     */
    public function add($id, Request $request, EntityManagerInterface $em)
    {
        /** @var User $user */
        $user = $this->getUser();
        // Récupération ou création de l'objet Ritournelle
        /** @var RitournelleRepository */
        $ritournelleRepo = $em->getRepository(Ritournelle::class);
        if ($id) {
            $ritournelle = $ritournelleRepo->find($id);
            if (!$ritournelle) {
                throw $this->createNotFoundException('impossible to find this ritournelle');
            }
        } else {
            $ritournelle = new Ritournelle();
        }

        $ritournelleForm = $this->createForm(RitournelleType::class, $ritournelle);

        $ritournelleForm->handleRequest($request);

        if ($ritournelleForm->isSubmitted() && $ritournelleForm->isValid()) {
            if ($id) {
                $this->addFlash('success', 'success.add_ritournelle');
            } else {
                $this->addFlash('success', 'success.update_ritournelle');
            }
            $ritournelle->setUser($user);
            $em->persist($ritournelle);
            $em->flush();

            if ($this->isGranted('ROLE_ADMIN')) {
                return $this->redirectToRoute('ritournelle_list');
            }

            return $this->redirectToRoute('ritournelle_add');
        }

        $ritournelles = $ritournelleRepo->findByUser($user);

        return $this->render(
            'ritournelle/add.html.twig',
            [
                'ritournelleForm' => $ritournelleForm->createView(),
                'ritournelles' => $ritournelles,
            ]
        );
    }
}
