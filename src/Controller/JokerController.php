<?php

namespace App\Controller;

use App\Entity\JokerPlayed;
use App\Entity\User;
use App\Service\GameManager;
use App\Service\JokerManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/joker", name="joker_")
 */
class JokerController extends AbstractController
{
    /**
     * @Route("/{id}", name="play", methods={"POST"})
     */
    public function play(
        User $user,
        EntityManagerInterface $entityManager,
        GameManager $gameManager,
        JokerManager $jokerManager,
        Request $request
    ) {
        /** @var User $user */
        if ($user != $this->getUser()) {
            throw $this->createAccessDeniedException('That\'s not your game!');
        }
        $game = $gameManager->getTodaysGame();
        $joker = $request->get('joker');

        // on vérifie si le joker a déjà été joué
        // si oui, on renvoie une erreur
        if ($game->getJokerPlayedBy($user, $joker)) {
            throw $this->createAccessDeniedException('You already played this joker');
        }

        // si non, on calcule l'indice
        try {
            $clue = $jokerManager->getClue($joker, $user, $game);
        } catch (\Exception $exception) {
            throw $this->createAccessDeniedException('You can\'t play this joker');
        }

        // on l'enregistre en base
        $jokerPlayed = new JokerPlayed();
        $jokerPlayed
            ->setGame($game)
            ->setJoker($joker)
            ->setUser($user)
            ->setClue($clue);
        $entityManager->persist($jokerPlayed);
        $entityManager->flush();

        // on renvoie l'indice
        return $this->json(['clue' => $clue, 'joker' => $joker]);
    }
}
