<?php

namespace App\Service;

use App\Controller\UserController;
use App\Entity\CustomRitournelle;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class CustomRitournelleManager
{
    private EntityManagerInterface $entityManager;
    private Environment $twigEnvironment;
    private GameManager $gameManager;
    private LoggerInterface $logger;
    private \Swift_Mailer $mailer;
    private TokenGeneratorInterface $tokenGenerator;
    private TranslatorInterface $translator;

    public function __construct(
        EntityManagerInterface $entityManager,
        Environment $twigEnvironment,
        GameManager $gameManager,
        LoggerInterface $logger,
        \Swift_Mailer $mailer,
        TokenGeneratorInterface $tokenGenerator,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->twigEnvironment = $twigEnvironment;
        $this->gameManager = $gameManager;
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->translator = $translator;
    }

    /**
     * @throws \Html2Text\Html2TextException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function notify(
        CustomRitournelle $customRitournelle,
        User $user
    ) {
        if (!$user->getToken()) {
            // on génère un token
            $user->setToken($this->tokenGenerator->generateToken());
            $this->entityManager->flush();
        }
        $tokenPlay = base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::PLAY_NEXT]));
        $tokenRanking = base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::RANKING_NEXT]));
        $emailParams = [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'customRitournelle' => $customRitournelle,
            'token_play' => $tokenPlay,
            'token_ranking' => $tokenRanking,
        ];
        $html = $this->twigEnvironment->render(
            'emails/warnNewCustom.html.twig',
            $emailParams
        );
        // On envoie le mail
        $message = (new \Swift_Message($this->translator->trans('mail.warnCustom.subject', ['%name%' => $customRitournelle->getUser()->getUsername()], 'mail')))
            ->setFrom('contact@notifications.ritournelle.eu', 'Carlos')
            ->setReplyTo('carlos@shauri.fr')
            ->setTo($user->getEmail())
            ->setBody(
                $html,
                'text/html'
            )
            /* If you also want to include a plaintext version of the message */
            ->addPart(
                Html2Text::convert($html),
                'text/plain'
            );
        $this->mailer->send($message);
    }
}
