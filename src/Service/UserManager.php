<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment as Twig_Environment;

class UserManager
{
    /** @var LoggerInterface */
    private $logger;
    /** @var \Swift_Mailer */
    private $mailer;
    /** @var Twig_Environment */
    private $twigEnvironment;
    /** @var TranslatorInterface */
    private $translator;
    /** @var TokenGeneratorInterface */
    private $tokenGenerator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        LoggerInterface $logger,
        \Swift_Mailer $mailer,
        Twig_Environment $twigEnvironment,
        TranslatorInterface $translator,
        TokenGeneratorInterface $tokenGenerator,
        EntityManagerInterface $entityManager
    ) {
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->twigEnvironment = $twigEnvironment;
        $this->translator = $translator;
        $this->tokenGenerator = $tokenGenerator;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \Html2Text\Html2TextException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws TokenNotFoundException
     */
    public function sendChangePassword(User $user)
    {
        // on génère un token si besoin
        if (!$user->getToken()) {
            $user->setToken($this->tokenGenerator->generateToken());
            $this->entityManager->flush();
        }

        $html = $this->twigEnvironment->render(
            'emails/changePassword.html.twig',
            [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'username' => $user->getUsername(),
                'token' => base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken()])),
            ]
        );
        // On envoie un mail à chaque utilisateur
        $message = (new \Swift_Message($this->translator->trans('user.email.change.password.subject', [], 'user')))
            ->setFrom('contact@notifications.ritournelle.eu', 'Carlos')
            ->setBcc('carlos@shauri.fr')
            ->setReplyTo('carlos@shauri.fr')
            ->setTo($user->getEmail())
            ->setBody(
                $html,
                'text/html'
            )
            /* If you also want to include a plaintext version of the message */
            ->addPart(
                Html2Text::convert($html),
                'text/plain'
            );
        $this->mailer->send($message);
    }
}
