<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\Result;
use App\Repository\GameRepository;
use App\Repository\ResultRepository;
use Doctrine\ORM\EntityManagerInterface;

class GameManager
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Result[] */
    private $currentWeekResults;

    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $firstCurrentWeek = null;
    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $secondCurrentWeek = null;
    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $thirdCurrentWeek = null;
    /**
     * il est essentiel de laisser null et non pas [].
     *
     * @var array|null
     */
    private $othersCurrentWeek = null;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getFirstCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->firstCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->firstCurrentWeek;
    }

    public function getSecondCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->secondCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->secondCurrentWeek;
    }

    public function getThirdCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->thirdCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->thirdCurrentWeek;
    }

    public function getOthersCurrentWeek(): array
    {
        // il est essentiel de laisser is_null et non pas []
        if (is_null($this->othersCurrentWeek)) {
            $this->calculateCurrentWeek();
        }

        return $this->othersCurrentWeek;
    }

    public function getFirstCurrentMonth(): array
    {
        return [];
    }

    public function getSecondCurrentMonth(): array
    {
        return [];
    }

    public function getThirdCurrentMonth(): array
    {
        return [];
    }

    public function getOthersCurrentMonth(): array
    {
        return [];
    }

    /**
     * @return Result[]
     */
    public function getCurrentWeekResults(): array
    {
        if (empty($this->currentWeekResults)) {
            /** @var ResultRepository $resultRepository */
            $resultRepository = $this->entityManager->getRepository(Result::class);
            $this->currentWeekResults = $resultRepository->findCurrentWeek();
        }

        return $this->currentWeekResults;
    }

    public function getTodaysGame(): Game
    {
        /** @var GameRepository $gameRepo */
        $gameRepo = $this->entityManager->getRepository(Game::class);

        return $gameRepo->getTodaysGame();
    }

    public function getPreviousGame(): ?Game
    {
        /** @var GameRepository $gameRepo */
        $gameRepo = $this->entityManager->getRepository(Game::class);

        return $gameRepo->getPreviousGame();
    }

    private function calculateCurrentWeek(): array
    {
        $ranking = $this->calculateRanking(
            $this->getCurrentWeekResults()
        );

        $this->firstCurrentWeek = $ranking['1'];
        $this->secondCurrentWeek = $ranking['2'];
        $this->thirdCurrentWeek = $ranking['3'];
        $this->othersCurrentWeek = $ranking['others'];

        return $ranking;
    }

    public function calculateLastWeek(): array
    {
        $resultRepository = $this->entityManager->getRepository(Result::class);

        $ranking = $this->calculateRanking(
            $resultRepository->findLastWeek()
        );

        return $ranking;
    }

    private function calculateRanking(array $results): array
    {
        $calculatedResults = [];
        $ranking = [
            '1' => [], '2' => [], '3' => [], 'others' => []
        ];

        foreach ($results as $result) {
            $user = $result->getUser();
            $idUser = $user->getId();
            $points = $result->getPoint();
            if (!array_key_exists($idUser, $calculatedResults)) {
                $calculatedResults[$idUser] = ['user' => $user, 'points' => 0];
            }
            $calculatedResults[$idUser]['points'] += $points;
        }
        usort($calculatedResults, function ($a, $b) {
            return $a['points'] <=> $b['points'];
        });

        while (!empty($calculatedResults)) {
            $entry = array_pop($calculatedResults);
            if (empty($ranking['1']) || $ranking['1'][0]['points'] == $entry['points']) {
                $ranking['1'][] = $entry;
                continue;
            }
            if (empty($ranking['2']) || $ranking['2'][0]['points'] == $entry['points']) {
                $ranking['2'][] = $entry;
                continue;
            }
            if (empty($ranking['3']) || $ranking['3'][0]['points'] == $entry['points']) {
                $ranking['3'][] = $entry;
                continue;
            }
            $ranking['others'][] = $entry;
        }

        return $ranking;
    }
}
