<?php

namespace App\Service;

use App\Entity\Game;
use App\Entity\JokerPlayed;
use App\Entity\Response;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class JokerManager
{
    private LoggerInterface $logger;

    private TranslatorInterface $translator;

    public function __construct(
        LoggerInterface $logger,
        TranslatorInterface $translator
    ) {
        $this->logger = $logger;
        $this->translator = $translator;
    }

    public function getClue(int $joker, User $user, Game $game): string
    {
        switch ($joker) {
            case JokerPlayed::JOKER_SIMILARITY:
                // on s'assure que le joueur a déjà un essai pour jouer ce joker
                $trials = $user->getTrials($game);
                if (empty($trials)) {
                    $this->logger->error(
                        'Unable to play Joker',
                        [
                            'nbTrials' => count($trials),
                            'idJoker' => $joker,
                            'idUser' => $user->getId(),
                            'idGame' => $game->getId(),
                        ]
                    );
                    throw new \Exception('Unable to play Joker');
                }

                return $this->closestTrial($trials, $game);
            case JokerPlayed::JOKER_RANDOM_WORD:
                $randomWords = $game->getRitournelle()->getRandomHalfWords();

                return $this->translator->trans(
                    'ritournelle.joker.clue.randomWord',
                    [
                        'response' => implode(', ', $randomWords),
                        'nb' => count($randomWords),
                    ],
                    'ritournelle');
            default:
                $this->logger->error(
                    'Unknown Joker',
                    [
                        'idJoker' => $joker,
                        'idUser' => $user->getId(),
                        'idGame' => $game->getId(),
                    ]
                );
                throw new \Exception('Unknwon Joker');
        }
    }

    private function closestTrial(array $trials, Game $game): string
    {
        /** @var Response[] $trials */
        $clue = '';
        $bestSimilary = 0;
        foreach ($trials as $trial) {
            $similarity = $game->getRitournelle()->getSimilarity($trial->getResponse());

            if ($similarity > $bestSimilary) {
                $bestSimilary = $similarity;
                $clue = $this->translator->trans('ritournelle.joker.clue.similarity', ['response' => $trial->getResponse(), 'similarity' => $similarity], 'ritournelle');
            }
        }

        return $clue;
    }
}
