<?php

namespace App\Command;

use App\Controller\UserController;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment as Twig_Environment;

class RitournelleCheckAcceptedWarnNewCommand extends Command
{
    protected static $defaultName = 'ritournelle:check-accepted-warn-new';

    /** @var LoggerInterface */
    private $logger;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var \Swift_Mailer */
    private $mailer;
    /** @var Twig_Environment */
    private $twigEnvironment;
    /** @var TranslatorInterface */
    private $translator;
    /** @var TokenGeneratorInterface */
    private $tokenGenerator;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        Twig_Environment $twigEnvironment,
        TranslatorInterface $translator,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->twigEnvironment = $twigEnvironment;
        $this->translator = $translator;
        $this->tokenGenerator = $tokenGenerator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Warns every user that a new Ritournelle is available')
            ->addArgument(
                'user',
                InputArgument::OPTIONAL | InputArgument::IS_ARRAY,
                'List of User to send the email'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $usersId = $input->getArgument('user');

//        $this->translator->setLocale("fr");
        // On récupère la liste des utilisateurs
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        if (!empty($usersId)) {
            $users = $userRepository->findById($usersId);
        } else {
            $users = $userRepository->findBy(['acceptedWarn' => true]);
        }
        foreach ($users as $user) {
            if (!$user->getToken()) {
                // on génère un token
                $user->setToken($this->tokenGenerator->generateToken());
                $this->entityManager->flush();
            }
            $html = $this->twigEnvironment->render(
                'emails/checkAcceptedWarnNew.html.twig',
                [
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                    'token' => base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::CHANGE_WARN_NEXT])),
                    'token_play' => base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::PLAY_NEXT])),
                ]
            );
            // On envoie un mail à chaque utilisateur
            $message = (new \Swift_Message($this->translator->trans('mail.check.warn.subject', [], 'mail')))
                ->setFrom('contact@notifications.ritournelle.eu', 'Carlos')
                ->setReplyTo('carlos@shauri.fr')
                ->setTo($user->getEmail())
                ->setBody(
                    $html,
                    'text/html'
                )
                /* If you also want to include a plaintext version of the message */
                ->addPart(
                    Html2Text::convert($html),
                    'text/plain'
                );
            $this->mailer->send($message);
            $io->note('un email a été envoyé à '.$user->getName());
        }

        $io->success(count($users).' mails ont été envoyés.');

        return 0;
    }
}
