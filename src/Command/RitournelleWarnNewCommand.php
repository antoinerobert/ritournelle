<?php

namespace App\Command;

use App\Controller\UserController;
use App\Entity\Game;
use App\Entity\User;
use App\Repository\GameRepository;
use App\Repository\UserRepository;
use App\Service\GameManager;
use Doctrine\ORM\EntityManagerInterface;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class RitournelleWarnNewCommand extends Command
{
    protected static $defaultName = 'ritournelle:warn-new';

    private EntityManagerInterface $entityManager;
    private Environment $twigEnvironment;
    private GameManager $gameManager;
    private LoggerInterface $logger;
    private \Swift_Mailer $mailer;
    private TokenGeneratorInterface $tokenGenerator;
    private TranslatorInterface $translator;

    public function __construct(
        EntityManagerInterface $entityManager,
        Environment $twigEnvironment,
        GameManager $gameManager,
        LoggerInterface $logger,
        \Swift_Mailer $mailer,
        TokenGeneratorInterface $tokenGenerator,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->twigEnvironment = $twigEnvironment;
        $this->gameManager = $gameManager;
        $this->logger = $logger;
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->translator = $translator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Warns every user that a new Ritournelle is available')
            ->addArgument('user', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'List of User to send the email')
        ;
    }

    /**
     * @return int
     *
     * @throws \Html2Text\Html2TextException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $usersId = $input->getArgument('user');

        // $this->translator->setLocale("fr");
        // On récupère la liste des utilisateurs
        /** @var UserRepository $userRepository */
        $userRepository = $this->entityManager->getRepository(User::class);
        if (!empty($usersId)) {
            $users = $userRepository->findById($usersId);
        } else {
            $users = $userRepository->findBy(['acceptedWarn' => true]);
        }

        /** @var GameRepository $gameRepo */
        $gameRepo = $this->entityManager->getRepository(Game::class);
        $game = $gameRepo->getTodaysGame();
        $ritournelle = $game->getRitournelle();
        
        if (date('w') == 1) {
            // Si nous sommes lundi, calculer le classement de la semaine précédente
            $lastWeekRanking = $this->gameManager->calculateLastWeek();
            $first = $lastWeekRanking['1'];
            $second = $lastWeekRanking['2'];
            $third = $lastWeekRanking['3'];
        } else {
            // Sinon on récupère le classement de la semaine en cours
            $lastWeekRanking = null;
            $first = $this->gameManager->getFirstCurrentWeek();
            $second = $this->gameManager->getSecondCurrentWeek();
            $third = $this->gameManager->getThirdCurrentWeek();
        }

        foreach ($users as $user) {
            if (!$user->getToken()) {
                // on génère un token
                $user->setToken($this->tokenGenerator->generateToken());
                $this->entityManager->flush();
            }
            $html = $this->twigEnvironment->render(
                'emails/warnNew.html.twig',
                [
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                    'token' => base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::CHANGE_WARN_NEXT])),
                    'token_play' => base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::PLAY_NEXT])),
                    'token_ranking' => base64_encode(serialize(['id' => $user->getId(), 'token' => $user->getToken(), 'next' => UserController::RANKING_NEXT])),
                    'ritournelle' => $ritournelle,
                    'firstCurrentWeek' => $first,
                    'secondCurrentWeek' => $second,
                    'thirdCurrentWeek' => $third,
                    'lastWeekRanking' => $lastWeekRanking,
                ]
            );
            // On envoie un mail à chaque utilisateur
            $message = (new \Swift_Message($this->translator->trans('mail.warn.subject', [], 'mail')))
                ->setFrom('contact@notifications.ritournelle.eu', 'Carlos')
                ->setReplyTo('carlos@shauri.fr')
                ->setTo($user->getEmail())
                ->setBody(
                    $html,
                    'text/html'
                )
                /* If you also want to include a plaintext version of the message */
                ->addPart(
                    Html2Text::convert($html),
                    'text/plain'
                );
            $this->mailer->send($message);
            $io->note('un email a été envoyé à '.$user->getName());
        }

        $io->success(count($users).' mails ont été envoyés.');

        return 0;
    }
}
