<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'user.username',
                    'required' => true,
                    'attr' => ['maxlength' => 250],
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'firstName',
                TextType::class,
                [
                    'label' => 'user.first_name',
                    'required' => false,
                    'attr' => ['maxlength' => 250],
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'label' => 'user.last_name',
                    'required' => false,
                    'attr' => ['maxlength' => 250],
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'user.email',
                    'required' => true,
                    'attr' => ['maxlength' => 250],
                    'translation_domain' => 'user',
                ]
            )
            ->add(
                'password',
                RepeatedType::class,
                [
                    'required' => true,
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label' => 'user.password',
                        'translation_domain' => 'user',
                    ],
                    'second_options' => [
                        'label' => 'user.second_password',
                        'translation_domain' => 'user',
                    ],
                ]
            )
          ->add(
            'save',
            SubmitType::class,
            [
              'label' => 'user.register',
              'translation_domain' => 'user',
            ]
          );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => User::class,
            ]
        );
    }
}
