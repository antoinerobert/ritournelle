<?php

namespace App\Form;

use App\Entity\CustomRitournelle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomRitournelleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'enigma',
                TextType::class,
                [
                    'label' => 'ritournelle.enigma',
                    'required' => true,
                    'attr' => ['maxlength' => 1024],
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'response',
                TextType::class,
                [
                    'label' => 'ritournelle.response',
                    'required' => true,
                    'attr' => ['maxlength' => 1024],
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'clue',
                TextType::class,
                [
                    'label' => 'ritournelle.clue',
                    'required' => true,
                    'attr' => ['maxlength' => 1024],
                    'translation_domain' => 'ritournelle',
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'ritournelle.defy',
                    'translation_domain' => 'ritournelle',
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CustomRitournelle::class,
        ]);
    }
}
