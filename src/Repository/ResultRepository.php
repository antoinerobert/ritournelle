<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Result;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

/**
 * @method Result|null find($id, $lockMode = null, $lockVersion = null)
 * @method Result|null findOneBy(array $criteria, array $orderBy = null)
 * @method Result[]    findAll()
 * @method Result[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Result[]    findByGame(Game $game)
 */
class ResultRepository extends EntityRepository
{
    /**
     * @return Result|null
     */
    public function findTodayResult(User $user, Game $game)
    {
        $result = $this->createQueryBuilder('r')
            ->andWhere('r.user = :user')
            ->andWhere('r.game = :game')
            ->setParameter('user', $user)
            ->setParameter('game', $game)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return array_pop($result);
    }

    /**
     * @throws ORMException
     */
    public function save(Game $game, User $user): Result
    {
        $result = new Result();
        $result->setGame($game);
        $result->setUser($user);
        $points = $user->getBasePoints($game) + $game->getNextPoints();
        $result->setPoint($points);

        $this->getEntityManager()->persist($result);
        $this->getEntityManager()->flush();

        return $result;
    }

    private function findByPeriod($start, $end)
    {
        return $this->createQueryBuilder('r')
            ->where('r.replyAt >= :start')
            ->andWhere('r.replyAt <= :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getQuery()
            ->getResult();
    }

    public function findCurrentWeek()
    {
        $startWeek = date('Y-m-d 00:00:00', strtotime('monday this week'));
        $endWeek = date('Y-m-d 23:59:59', strtotime('sunday this week'));

        return $this->findByPeriod($startWeek, $endWeek);
    }

    public function findLastWeek()
    {
        $startWeek = date('Y-m-d 00:00:00', strtotime('monday last week'));
        $endWeek = date('Y-m-d 23:59:59', strtotime('sunday last week'));

        return $this->findByPeriod($startWeek, $endWeek);
    }
}
