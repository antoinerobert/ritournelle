<?php

namespace App\Repository;

use App\Entity\CustomRitournelle;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * @method CustomRitournelle|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomRitournelle|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomRitournelle[]    findAll()
 * @method CustomRitournelle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomRitournelleRepository extends EntityRepository
{
    /**
     * @return CustomRitournelle[]
     */
    public function findOthers(\DateTimeImmutable $dateTimeImmutable, User $user)
    {
        return $this->createQueryBuilder('cr')
            ->andWhere('cr.createdAt = :today')
            ->andWhere('cr.user <> :user')
            ->setParameter('today', $dateTimeImmutable)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
