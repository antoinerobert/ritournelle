<?php

namespace App\Repository;

use App\Entity\Game;
use App\Entity\Ritournelle;
use Doctrine\ORM\EntityRepository;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends EntityRepository
{
    /**
     * @return Game
     *
     * @throws \Exception
     */
    public function getTodaysGame()
    {
        $query = $this->createQueryBuilder('g')
            ->andWhere('g.playedAt = :today')
            ->setParameter('today', new \DateTime('today'));
        $result = $query->getQuery()->getResult();
        if (!empty($result)) {
            return array_pop($result);
        }
        // Aucune Ritournelle n'a été encore jouée aujourd'hui
        // on en choisie une au hasard qui n'a pas été jouée depuis 1 mois
        /** @var RitournelleRepository $ritournelleRepo */
        $ritournelleRepo = $this->getEntityManager()->getRepository(Ritournelle::class);
        $times = 1;
        do {
            $ritournelles = $ritournelleRepo->findPlayedLessThanXTimesAndNotPlayedSince($times, '-2 month');
            ++$times;
        } while (count($ritournelles) < 5);
        $index = rand(0, count($ritournelles) - 1);
        $ritournelle = $ritournelles[$index];

        $game = new Game();
        $game->setPlayedAt(new \DateTimeImmutable('today'));
        $game->setRitournelle($ritournelle);
        $this->getEntityManager()->persist($game);
        $this->getEntityManager()->flush();

        return $game;
    }

    /**
     * @return Game|null
     *
     * @throws \Exception
     */
    public function getPreviousGame()
    {
        $query = $this->createQueryBuilder('g')
            ->andWhere('g.playedAt < :today')
            ->orderBy('g.playedAt', 'desc')
            ->setParameter('today', new \DateTime('today'));
        $results = $query->getQuery()->getResult();

        return array_shift($results);
    }
}
