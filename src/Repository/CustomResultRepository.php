<?php

namespace App\Repository;

use App\Entity\CustomResult;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * @method CustomResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomResult[]    findAll()
 * @method CustomResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomResultRepository extends EntityRepository
{
    /**
     * @return CustomResult[]
     */
    public function findResults(\DateTimeImmutable $dateTimeImmutable, User $user)
    {
        return $this->createQueryBuilder('cr')
            ->andWhere('cr.replyAt >= :today')
            ->andWhere('cr.user = :user')
            ->setParameter('today', $dateTimeImmutable)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
