<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181202172110 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE custom_result (id INT AUTO_INCREMENT NOT NULL, custom_ritournelle_id INT NOT NULL, user_id INT NOT NULL, reply_at DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX IDX_27C71517F98F090C (custom_ritournelle_id), INDEX IDX_27C71517A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE custom_result ADD CONSTRAINT FK_27C71517F98F090C FOREIGN KEY (custom_ritournelle_id) REFERENCES custom_ritournelle (id)');
        $this->addSql('ALTER TABLE custom_result ADD CONSTRAINT FK_27C71517A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE custom_result');
    }
}
